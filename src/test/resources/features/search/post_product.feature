Feature: Search for the product

### Please use endpoint GET https://waarkoop-server.herokuapp.com/api/v1/search/demo/{product} for getting the products.
### Available products: "orange", "apple", "pasta", "cola"
### Prepare Positive and negative scenarios

  Scenario Outline: Valid response for '/search/demo/{product}' endpoint
    When User calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/demo/<products>"
    Then he sees that response code is 200
    Then he sees response header 'Content-Type' is JSON
    Then he sees response body not empty
    Then he sees 'provider' fields from response body not empty or null string
    Then he sees 'title' fields from response body contains "<products>" substring
    Then he sees 'url' fields from response body not empty or null string
    Then he sees 'brand' fields from response body not empty or null string
    Then he sees 'price' fields from response body not less than 0
    Then he sees 'unit' fields from response body not empty or null string
    Then he sees 'promoDetails' fields from response body are not null values
    Then he sees 'image' fields from response body not empty or null string
    Examples:
      | products |
      | orange   |
      | apple    |
      | pasta    |
      | cola     |


    Scenario Outline: Invalid response for '/search/demo/{product}' endpoint
      When User calls endpoint "https://waarkoop-server.herokuapp.com/api/v1/search/demo/<products>"
      Then he sees that response code is 404
      And he sees response header 'Content-Type' is JSON
      Then he sees response body not empty
      And he sees response body contains error message for product "<products>"
      Examples:
        | products |
        | mango    |
        | avocado  |
        | fanta    |
        | plumbus  |