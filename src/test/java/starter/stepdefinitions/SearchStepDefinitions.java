package starter.stepdefinitions;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.http.ContentType;
import net.serenitybdd.rest.SerenityRest;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.hamcrest.Matchers.*;

public class SearchStepDefinitions {


	@When("User calls endpoint {string}")
	public void UserCallsEndpoint(String url) {
		SerenityRest
				.given()
				.contentType(ContentType.JSON)
				.get(url);
	}

	@Then("he sees that response code is {int}")
	public void heSeesThatResponseCodeIs200(int statusCode) {
		restAssuredThat(response ->
				                response.statusCode(statusCode));
	}

	@Then("he sees response header 'Content-Type' is JSON")
	public void heSeesResponseHeaderContentTypeIsJSON() {
		restAssuredThat(response ->
				                response.contentType(ContentType.JSON));
	}

	@Then("he sees response body not empty")
	public void heSeesResponseBodyNotEmpty() {
		restAssuredThat(response ->
				                response.body("", not(empty())));
	}

	@Then("he sees {string} fields from response body not empty or null string")
	public void heSeesFieldsFromResponseBodyNotEmptyOrNullString(String fieldName) {
		restAssuredThat(response ->
				                response.body(fieldName, not(emptyOrNullString())));
	}

	@Then("he sees 'title' fields from response body contains {string} substring")
	public void heSeesTitleFieldsFromResponseBodyContainsSubstring(String product) {
		restAssuredThat(response ->
				                hasItem(containsStringIgnoringCase((product))));
	}

	@Then("he sees 'price' fields from response body not less than 0")
	public void heSeesPriceFieldsFromResponseBodyNotLessThan() {
		restAssuredThat(response ->
				                response.body("price", not(lessThan(0))));
	}

	@Then("he sees 'isPromo' fields from response body are boolean")
	public void heSeesUnitFieldsFromResponseBodyAreBoolean() {
		restAssuredThat(response ->
				                everyItem(is(instanceOf(Boolean.class))));
	}


	@Then("he sees 'promoDetails' fields from response body are not null values")
	public void heSeesPromoDetailsFieldsFromResponseBodyAreNotNullValues() {
		restAssuredThat(response ->
				                response.body("promoDetails", hasItem(not(nullValue()))));
	}

	@Then("he sees response body contains error message for product {string}")
	public void heSeesResponseBodyContainsErrorMessageForProduct(String product) {
		restAssuredThat(response -> {
			response.body("detail.error", is(true));
			response.body("detail.message", equalTo("Not found"));
			response.body("detail.requested_item", equalTo(product));

			// looks like a side service provider link. If that is so - not necessary to verify
			response.body("detail.served_by", equalTo("https://waarkoop.com"));
		});
	}
}