# Technology stack
- Java
- Serenity
- Cucumber
- Maven
- RestAssured

## Before start:
- Install JDK 1.8 or higher
- Install Maven 3 or higher
- Install any IDE for Java e.g. Intellij IDEA


# Get started
* Copy repository URL
* Clone repository `git clone <url>`
* Open project by IDE
* Run tests `mvn clean verify`
* Open HTML report. Find a link to a report at the end of test run. Or manually open:
    * `target` folder => `site` folder => `index.html`

### What was refactored/removed/updated all over the project
- removed Gradle integration - because Maven required
- removed "history" package. Redundant package
- .github package. Because GitLab CI integration required instead
- removed CarsAPI.class. Unused redundant class
- removed src/main package because this is not necessary for a demonstration project of this scale. This package can 
be added later for the next development stages of the current project
- serenity.conf - removed because this file provided the webdriver configuration. This is not necessary for a 
demonstration project of this scale. Can be added later for storing environment variables or other configurations
- .gitignore - updated regarding project needs and actual state

### Refactored/removed/updated in pom.xml:
* groupId - updated
* artifactId - updated
* name - updated
* properties:
  - tags - not used for current project
  - webdriver - not used for current project. For UI tests required
* maven-failsafe-plugin:
  - systemPropertyVariables 
    - webdriver.base.url - not used. For UI tests required
* net.serenity-bdd.maven.plugins:
  - configuration
    - tags - not used for current project
* dependencies:
  - serenity-screenplay-webdriver - dependency required for UI tests. Not actual for API tests
  - assertj-core - removed because I didn't use it in my demo project scale
  - **!! hamcrest-all** - I've deleted it because this is a transitive dependency for RestAssured. 
But if the project's coding practice requires explicit dependencies for better readability, leave them by updating the 
  version. In addition - current project doesn't have dependency conflicts. But in case project faces such an 
  issue - explicit dependency must be specified.
  - logback.classic.version - updated to a newer version due to a vulnerability in the old version:
    `logback.classic.version - Dependency maven:ch.qos.logback:logback-classic:1.0.13 is vulnerable
    CVE-2017-5929 9.8 Deserialization of Untrusted Data vulnerability pending CVSS allocation
    CVE-2021-42550 6.6 Deserialization of Untrusted Data vulnerability pending CVSS allocation`
* credentials MAVEN_REPO_URL, MAVEN_REPO_USER and MAVEN_REPO_PASS are not provided. Usually these variables are set 
as environment variables on the local computer or as properties in a project build system such as Jenkins, 
and are used when interacting with the repository. `distributionManagement` section has been removed 
until the requirements are clarified. As well as `settings.xml` from .m2 folder.

### Feature file and steps
**Feature** file [post_product.feature](src%2Ftest%2Fresources%2Ffeatures%2Fsearch%2Fpost_product.feature) reworked.
Cucumber Scenario changed for Scenario Outline. This is a scenario with parameterized input data. It allows you to 
run the same scenario with different data sets. <br>
Two Scenario Outlines provided. One positive. One negative.
<br>

**Steps** are reworked. One action - one step. For current demo project of this scale - this is ok.
<br>
**_My goal was to make scenarios transparent at first glance. Without going into the details of the internal 
implementation of the steps._**
<br>


### My thoughts on how to further expand and develop the project
1. In real project scale - nice to combine steps into actions. Additional layer can be added and HTML reporter changed.
Because actions are not properly displayed in default HTML reporter.
2. BaseURL should not be hardcoded or sent from feature file. It should be stored in special config file 
(e.g. serenity.conf) as constant
3. Environment variables should also be stored in one place
4. API endpoints should be stored as constants separately for test code
5. API endpoints should be divided by API version
6. RestAssured should have separate class with config and methods
7. I offer to update all dependencies to the latest versions because of plenty vulnerabilities with current versions
8. Project Lombok can be added for better testing experience. POJO classes can be added for request/response bodies
9. AssertJ can be added later for better assertion experience
10. Change HTML reporter
11. src/main package can be added later
12. Clarified requirements regarding `distributionManagement` at pom.xml
13. Clarified requirements regarding `settings.xml` for .m2 folder